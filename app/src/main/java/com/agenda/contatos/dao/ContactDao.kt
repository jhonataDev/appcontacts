package com.agenda.contatos.dao

import androidx.room.*
import com.agenda.contatos.model.Contatos

@Dao
interface ContactDao {

    @Insert
    fun insertContact (contatos: Contatos)

    @Update
    fun updateContact (contatos: Contatos)

    @Delete
    fun deleteContact (contatos: Contatos)

    @Query("SELECT * FROM Contatos Order by nome")
    fun getContacts(): List<Contatos>

}