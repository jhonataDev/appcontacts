package com.agenda.contatos.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import com.agenda.contatos.R
import com.agenda.contatos.adapter.ContatosAdapter
import com.agenda.contatos.model.Contatos
import kotlinx.android.synthetic.main.activity_main_contatos.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_contatos)

        val recycleView = recyclerView
        recycleView.adapter = ContatosAdapter(listaTeste(), this)
        recycleView.layoutManager = LinearLayoutManager(this)

        btn_fab.setOnClickListener {
            val intent = Intent(this,FormActivity::class.java)
            startActivity(intent)
        }
    }

    private fun listaTeste() : List <Contatos>{
        return listOf(
            Contatos(id = 1, nome = "Jhonata", telefone = "(11)99999-8888"),
            Contatos(id = 2,nome = "Ernesto", telefone = "(11)92222-8888"),
            Contatos(id = 3,nome = "Jaimilton", telefone = "(11)93333-8888"),
            Contatos(id = 4,nome = "Ernesto", telefone = "(11)92222-8888"),
            Contatos(id = 5,nome = "Jaimilton", telefone = "(11)93333-8888"),
            Contatos(id = 6,nome = "Ernesto", telefone = "(11)92222-8888"),
            Contatos(id = 7,nome = "Jaimilton", telefone = "(11)93333-8888"),
            Contatos(id = 8,nome = "Ernesto", telefone = "(11)92222-8888"),
            Contatos(id = 9,nome = "Jaimilton", telefone = "(11)93333-8888"),
            Contatos(id = 10,nome = "Jandira", telefone = "(11)94444-8888")
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater = menuInflater
        inflater.inflate(R.menu.menu_search, menu)

        return super.onCreateOptionsMenu(menu)
    }
}