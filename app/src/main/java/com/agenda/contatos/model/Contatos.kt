package com.agenda.contatos.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Contatos(

    @PrimaryKey
    val id:Int,
    val nome: String,
    val telefone: String ) {
}