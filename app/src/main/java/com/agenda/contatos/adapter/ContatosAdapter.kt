package com.agenda.contatos.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.agenda.contatos.R
import com.agenda.contatos.model.Contatos
import kotlinx.android.synthetic.main.layout_recycleview.view.*


class ContatosAdapter (
    private val contatos: List<Contatos>,
    private val context: Context): RecyclerView.Adapter<ContatosAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_recycleview, parent, false )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contatos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contacts = contatos[position]
        holder.name.text = contacts.nome
        holder.phone.text = contacts.telefone
    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.tv_name_user
        val phone = itemView.tv_phone_number
    }

}