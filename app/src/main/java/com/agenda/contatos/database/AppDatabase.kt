package com.agenda.contatos.database

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.agenda.contatos.dao.ContactDao

abstract class AppDatabase : RoomDatabase(){
    abstract fun contactDao(): ContactDao

    companion object {
        var INSTANCE : AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase?{
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "Contatos").build()
                }
            }

            return INSTANCE
        }

        fun onDestroyDatabase(){
            INSTANCE = null
        }
    }
}